<?php
// Animal
require('animal.php');

$sheep = new sheep("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

// Frog
require('frog.php');

$frog = new frog("buduk");
echo "Name : " . $frog->name . "<br>"; 
echo "Legs : " . $frog->legs . "<br>"; 
echo "Cold Blooded : " . $frog->cold_blooded . "<br>"; 
echo "Jump : " . $frog->jump . "<br><br>"; // "hop hop"

// Ape
require('ape.php');

$ape = new ape("kera sakti");
echo "Name : " . $ape->name . "<br>"; 
echo "Legs : " . $ape->legs . "<br>"; 
echo "Cold Blooded : " . $ape->cold_blooded . "<br>"; 
echo "Jump : " . $ape->yell; // "hop hop"
?>